class Taberna constructor(val name: String, var oro:Int,var fatiga:Int,var sed:Int,var banco: Int) {
    fun show(){
        println("""
            $name: Hombre, estoy sediento. Me dirijo a la taberna.
            $name: Oh! Esto sí que está muy bueno, ha calmado mi sed.
        """.trimIndent())
    }

    fun Beber(){
        sed=sed-5
        show()
    }
}